package cz.sspbrno.team18;

import cz.sspbrno.team18.database.SQLiteDBConnector;
import cz.sspbrno.team18.objects.Point;
import cz.sspbrno.team18.objects.Polygon;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import javax.swing.*;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EditorController implements Initializable
{
    protected static SQLiteDBConnector dbConnector;
    public static ArrayList<Polygon> polygonArrayList;
    public static int lastIndex;

    public static Polygon polygon = null;
    @FXML
    private TextField xPos;
    @FXML
    private TextField yPos;
    @FXML
    private TextField name;
    @FXML
    private Canvas canvas;
    @FXML
    private MenuButton Polygon;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        lastIndex = -1;

        polygonArrayList = new ArrayList<>();
        try {
            dbConnector = new SQLiteDBConnector();
            polygonArrayList.addAll(dbConnector.load());
        } catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Chyba při načítání z databáze! : " + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }

        for (Polygon pol: polygonArrayList)
        {
            MenuItem mi = new MenuItem(pol.getName());
            mi.setOnAction(new myEventHandler(this));
            Polygon.getItems().add( mi );

        }

        drawAxes();

        System.out.println("Window initialize successfully");
    }


    public void drawAxes()
    {
        double width = canvas.getWidth();
        double height = canvas.getHeight();

        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setStroke(Color.WHITE);

        graphicsContext.setFill(Color.gray(0.90));
        graphicsContext.fillRect(0,0,canvas.getWidth(),canvas.getHeight());

        graphicsContext.setLineWidth(1.0);
        graphicsContext.setStroke(Color.BLACK);

        graphicsContext.beginPath();
        graphicsContext.moveTo(width/2,0);
        graphicsContext.lineTo(width/2,height);
        graphicsContext.stroke();

        graphicsContext.moveTo(0,height/2);
        graphicsContext.lineTo(width,height/2);
        graphicsContext.stroke();
    }

    /**
     * Adding new point to actual polygon
     * @param mouseEvent
     */
    public void onClickedAddPoint(MouseEvent mouseEvent)
    {
        if(polygon == null) {
            if (!name.getText().equals(""))
                polygon = new Polygon(name.getText());
            else
                polygon = new Polygon("Default Name");
        }
        try
        {
            Main.addNextToStack(polygon, new Point(Float.parseFloat(xPos.getText()), Float.parseFloat(yPos.getText())));
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Zadány chybné souřadnice! : " + e.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
        reDraw();
    }

    /**
     * This function will insert data to the database
     * @param mouseEvent
     */
    public void onClickedSendSql(MouseEvent mouseEvent) throws SQLException
    {
        onClickedNew(mouseEvent);
        dbConnector.save(polygonArrayList);
        polygon = null;
        reDraw();
    }

    /**
     * Draw all the polygons on the canvas
     */
    public void reDraw()
    {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setLineWidth(1.0);

        graphicsContext.setStroke(Color.WHITE);

        graphicsContext.setFill(Color.gray(0.90));
        graphicsContext.fillRect(0,0,canvas.getWidth(),canvas.getHeight());

        if(lastIndex != polygonArrayList.indexOf(polygon) && lastIndex != -1)
        {
            lastIndex = polygonArrayList.indexOf(polygon);
        }

        if(polygon == null && !polygonArrayList.isEmpty())
        {
            return;
        }

        graphicsContext.setStroke(Color.BLACK);
        drawAxes();
        drawPolygon(graphicsContext);
    }

    private void drawPolygon(GraphicsContext graphicsContext)
    {
        for (int i = 1; i < polygon.size(); i++)
        {
            graphicsContext.beginPath();
            Point lastPoint = Utils.calculateToDraw(polygon, canvas.getWidth(), canvas.getHeight()).get(i-1);
            Point nowPoint = Utils.calculateToDraw(polygon, canvas.getWidth(), canvas.getHeight()).get(i);
            graphicsContext.moveTo(lastPoint.getX(), lastPoint.getY());
            graphicsContext.lineTo(nowPoint.getX(), nowPoint.getY());
            graphicsContext.stroke();
        }
    }

    /**
     * Complete the pattern (set last point to x = 0, y = 0
     * @param mouseEvent
     */
    public void onClickedComplete(MouseEvent mouseEvent)
    {
        if (polygon != null)
        {
            if (polygon.size() > 2)
            {
                polygon.add(new Point(0, 0));
                reDraw();
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Nelze doplnit polygon!","Error",JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Creates new polygon
     * @param mouseEvent
     * @throws SQLException
     */
    public void onClickedNew(MouseEvent mouseEvent) throws SQLException
    {
        if (polygon == null)
            return;

        polygon.setName(name.getText());
        if(name.getText().equals(""))
            polygon.setName("Default polygon");
        String[] options = {"yes","no"};
        if (JOptionPane.showOptionDialog(null,"Chcete uložit aktuální změny? ","Volba", 0,1,null, options,"yes") == 0) //poup windows do you want to save data ?
        {
            polygonArrayList.add(polygon);
            MenuItem mi = new MenuItem(polygon.getName());
            mi.setOnAction(new myEventHandler(this));
            Polygon.getItems().add(mi);
        }
        polygon = null;
        reDraw();
        drawAxes();
    }



    /**
     * Reserved for future use
     * @param mouseEvent
     */
    public void onClickedCanvas(MouseEvent mouseEvent)
    {
        //System.err.println(mouseEvent.getX()+", "+mouseEvent.getY()); if(polygon == null) {
        if (polygon == null)
        {
            if (!name.getText().equals(""))
                polygon = new Polygon(name.getText());
            else
                polygon = new Polygon("Default Name");
        }

        Main.addNextToStack(polygon, Utils.calculateToSave(new Point((float)mouseEvent.getX(),(float)mouseEvent.getY()),canvas.getWidth(),canvas.getHeight())); //add try catch(ParseExeption)

        reDraw();
    }

    /**
     * Just for debuging
     * @param mouseEvent
     * @throws SQLException
     */
    public void onClickDebug(MouseEvent mouseEvent) throws SQLException
    {
        System.err.println("DEBUG: "+ dbConnector.load());
        reDraw();
    }

}
