package cz.sspbrno.team18;

import cz.sspbrno.team18.database.SQLiteDBConnector;
import cz.sspbrno.team18.objects.Point;
import cz.sspbrno.team18.objects.Polygon;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Application {

    private SQLiteDBConnector connector;
    private ArrayList<Polygon> polygons;
    private Scanner scn;

    public Application() throws Exception {
        polygons = new ArrayList<>();
        connector = new SQLiteDBConnector();
        scn = new Scanner(System.in);
    }

    public void start() throws SQLException {

        label:
        do {
            System.out.println("q - ukončení aplikace, c - vyvoření random polygonů,"+
                    " s - uložení databáze, l - načtení databáze, ostatní pro přeskočení");
            String input = scn.nextLine();
            switch (input) {
                case "q":
                    break label;
                case "s":
                    for (Polygon polygon : polygons) {
                        System.out.println("NAME: " + polygon.getName());
                        for (Point point : polygon) {
                            System.out.println(point);
                        }
                    }
                    connector.save(polygons);
                    break;
                case "l":
                    for (Polygon polygon : connector.load()) {
                        System.out.println("NAME: " + polygon.getName());
                        for (Point point : polygon) {
                            System.out.println(point);
                        }
                    }
                    break;
                case "c":
                    for(int i = 1; i < 3; i++ ) {
                        polygons.add(calculateRandomPoints(3));
                    }
                    break;
                    default:
                        polygons.add(inputPoints());
                        break ;
            }


        } while (true);
    }

    /**
     * Adding points from terminal
     * @return Stack of input Points
     */

    private Polygon inputPoints()
    {
        System.out.println("Zadej nazev polygonu: ");
        Polygon polygon = new Polygon(scn.nextLine());

        do
        {
            System.out.print("Zadej souřadnice bodu(q - pro ukonceni zadavani, X;Y - tvar zadavani): ");
            String input = scn.nextLine();

            if(input.equals("q")) {
                break;
            }

            String[] splitedArray = input.split(";");

            try
            {
                if(!addNextToStack(polygon, new Point(Float.parseFloat(splitedArray[0]), Float.parseFloat(splitedArray[1]))))
                    throw new Exception("addPointExecption");
            }
            catch (Exception e)
            {
                System.err.println("Chyba ve tvaru zadaného čísla: "+ e.getMessage());
            }
        } while (true);

        for(Point point:polygon){
            System.out.println(point);
        }
        return polygon;
    }

    /**
     * Calls calculateRandomPoints with multiplier = 100
     * @param count - Number of points what will be generated
     * @return - Stack of generated points
     */
    public Polygon calculateRandomPoints(int count)
    {
        return calculateRandomPoints(count, 100f);
    }

    /**
     * Generate random points
     * @param count - Number of points what will be generated
     * @param multiplier - Numbers will be multiplied by this number
     * @return Stack of random Points
     */
    public Polygon calculateRandomPoints(int count, float multiplier)
    {
        ThreadLocalRandom random = ThreadLocalRandom.current();

        Polygon points = new Polygon("Random polygon" + random.nextInt());

        for(int i =0; i < count; i++)
        {
            Point point = new Point(random.nextFloat() * multiplier, random.nextFloat() * multiplier);
            addNextToStack(points, point);
        }

        return points;
    }

    /**
     * Adding point to the stack safely
     * @param stack - Stack of point, can't be null
     * @param nextPoint - Next point in the stack can't be same as the last point
     * @return true if success, false if failure
     */
    public boolean addNextToStack(Polygon stack, Point nextPoint)
    {
        if(stack == null)
            return false;

        if(stack.size() == 0)
        {
            stack.push(nextPoint);
            return true;
        }
        Point lastPoint = stack.pop();
        stack.push(lastPoint);

        if(nextPoint.equals(lastPoint))
            return false;

        stack.push(nextPoint);

        return true;
    }
}
