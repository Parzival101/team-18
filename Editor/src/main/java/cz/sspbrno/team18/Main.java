package cz.sspbrno.team18;

import cz.sspbrno.team18.objects.Point;
import cz.sspbrno.team18.objects.Polygon;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.sql.SQLException;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main extends Application
{
    public static void main(String[] args)
    {

        launch(args); // Starts GUI of the Application

        Polygon pointStack;

        pointStack = inputPoints();

        for(Point point: pointStack)
        {
            System.out.println(point);
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("../../../../resources/Editor.fxml")); //ehm... don't mind about it, it works :D

        Scene scene = new Scene(root);

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try {
                    EditorController.dbConnector.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Adding points from terminal
     * @return Stack of input Points
     */

    public static Polygon inputPoints()
    {
        Scanner scn = new Scanner(System.in);
        System.out.println("Zadej nazev polygonu: ");
        Polygon points = new Polygon(scn.nextLine());

        do
        {
            System.out.print("Zadej souřadnice bodu(q - pro ukonceni zadavani, X;Y - tvar zadavani): ");
            String input = scn.nextLine();

            if(input.equals("q"))
                break;

            String[] splitedArray = input.split(";");

            try
            {
                if(!addNextToStack(points, new Point(Float.parseFloat(splitedArray[0]), Float.parseFloat(splitedArray[1]))))
                    throw new Exception("addPointExecption");
            }
            catch (Exception e)
            {
                System.err.println("Chyba ve tvaru zadaného čísla: "+ e.getMessage());
            }
        } while (true);
        return points;
    }

    /**
     * Calls calculateRandomPoints with multiplier = 100
     * @param count - Number of points what will be generated
     * @return - Stack of generated points
     */
    public static Polygon calculateRandomPoints(int count)
    {
        return calculateRandomPoints(count, 100f);
    }

    /**
     * Generate random points
     * @param count - Number of points what will be generated
     * @param multiplier - Numbers will be multiplied by this number
     * @return Stack of random Points
     */
    public static Polygon calculateRandomPoints(int count, float multiplier)
    {
        ThreadLocalRandom random = ThreadLocalRandom.current();

        Polygon points = new Polygon("Random polygon" + random.nextInt());

        for(int i =0; i < count; i++)
        {
            Point point = new Point(random.nextFloat() * multiplier, random.nextFloat() * multiplier);
            addNextToStack(points, point);
        }

        return points;
    }

    /**
     * Adding point to the stack safely
     * @param stack - Stack of point, can't be null
     * @param nextPoint - Next point in the stack can't be same as the last point
     * @return true if success, false if failure
     */
    public static boolean addNextToStack(Polygon stack, Point nextPoint)
    {
        if(stack == null)
            return false;

        if(stack.size() == 0)
        {
            stack.push(nextPoint);
            return true;
        }
        Point lastPoint = stack.pop();
        stack.push(lastPoint);

        if(nextPoint.equals(lastPoint))
            return false;

        stack.push(nextPoint);

        return true;
    }


}
