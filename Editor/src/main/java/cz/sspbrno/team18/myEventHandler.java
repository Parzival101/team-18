package cz.sspbrno.team18;

import cz.sspbrno.team18.objects.Polygon;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;


public class myEventHandler implements EventHandler<ActionEvent>
{
    private EditorController editorController;
    public myEventHandler(EditorController editorController)
    {
        this.editorController = editorController;
    }

    @Override
    public void handle(ActionEvent event)
    {
        String nameOfCaller = ((MenuItem)event.getSource()).getText();
        EditorController.lastIndex = EditorController.polygonArrayList.indexOf(EditorController.polygon);
        if(EditorController.lastIndex == -1 && !(EditorController.polygon == null))
        {
            EditorController.polygonArrayList.add(EditorController.polygon);
            EditorController.lastIndex = EditorController.polygonArrayList.indexOf(EditorController.polygon);
        }

        EditorController.polygon = getPolygonByName(nameOfCaller);
        editorController.reDraw();
    }

    private static Polygon getPolygonByName(String name)
    {
        for (Polygon pt:EditorController.polygonArrayList)
        {
            if(pt.getName().equals(name))
                return pt;
        }
        return null;
    }
}
