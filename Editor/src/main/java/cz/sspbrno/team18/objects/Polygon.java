package cz.sspbrno.team18.objects;

import java.util.Stack;

public class Polygon extends Stack<Point>
{
    private String name;

    /**
     * Creates polygon
     * @param name - Name of the polygon
     */
    public Polygon(String name)
    {
        this.name = name;
        this.push(new Point(0,0));
    }

    /**
     * Constructor only for draw feature
     */
    public Polygon()
    {
        this.name = "Draw";
    }

    /**
     *
     * @return name of the polygon
     */
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public synchronized String toString()
    {
        return name + " " + super.toString();
    }

}
