package cz.sspbrno.team18.objects;

public class Point
{
    private float x;
    private float y;

    /**
     * Creates point on coordinate X and Y
     * @param x - X coordinate of the point
     * @param y - Y coordinate of the point
     */
    public Point(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * @return X coordinate of the point
     */
    public float getX()
    {
        return this.x;
    }

    /**
     * @return Y coordinate of the point
     */
    public float getY()
    {
        return y;
    }

    /**
     * Calculates distance of two point using Pythagoras sentence
     * @param nextPoint - Point next to this caller point
     * @return Distance between this point and the nextPoint
     */
    public float distanceTo(Point nextPoint)
    {
        float distance = (float) Math.sqrt( Math.pow(nextPoint.getX() - this.x, 2) + Math.pow(nextPoint.getY() - this.y, 2) );
        return distance;
    }

    @Override
    public String toString()
    {
        return String.format("Point {X = %f ,Y = %f}", this.x, this.y);
    }
}
