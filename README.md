# Team 18

## Build instructions
### Requirements:
   * Java 1.8 with support JavaFX
   * Gradle build tool 
### Build:
* Use ```$ gradle build Editor``` to build editor
* Use ```$ gradle build Control``` to build control app

## Run:
* Run using ```$ java -jar nameOfApp.jar```
